/* eslint-disable @typescript-eslint/no-unused-vars */
const AdmZip = require('adm-zip');
import { EventEmitter } from 'events';

import type { ZipPluginCapacitor, ZipOptions, UnzipOptions, CallbackID } from '../../src/definitions';

export class Zip extends EventEmitter implements ZipPluginCapacitor {
  async zip(options: ZipOptions): Promise<CallbackID> {
    console.error("not implemented in electron", options);
    throw "not implemented in electron";
  }

  unzip(options: UnzipOptions): Promise<CallbackID> {
    return new Promise((resolve) => {
      const callbackId = `zc-${(new Date()).getTime()}`;
      resolve(callbackId);

      setTimeout(() => {
        if(!options) {
          this.emit(callbackId, null, "must set options");
          return;
        }
  
        const source = options.source;
        const dest = options.destination;
  
        if(!source || !dest) {
          this.emit(callbackId, null, "must set source and destination");
          return;
        }
  
        try {
          var zipfile = new AdmZip(source);
          //console.log("Start Extract");
          zipfile.extractAllTo(dest, true);
          this.emit(callbackId, {progress: 100, complete: true, path: dest}, null);
        } catch (error) {
          this.emit(callbackId, null, error);
        }
      }, 100);
    });
  }
}
