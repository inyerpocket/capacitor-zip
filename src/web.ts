import { WebPlugin } from '@capacitor/core';

import type { ZipPluginCapacitor, ZipOptions, UnzipOptions, CallbackID, ProgressCallback } from './definitions';

export class ZipWeb extends WebPlugin implements ZipPluginCapacitor {
  async zip(options: ZipOptions, callback: ProgressCallback): Promise<CallbackID> {
    console.error("not implemented in web", options);
    callback(null, "not implemented in web");
    return "";
  }

  async unzip(options: UnzipOptions, callback: ProgressCallback): Promise<CallbackID> {
    console.error("not implemented in web", options);
    callback(null, "not implemented in web");
    return "";
  }
}