import { registerPlugin } from '@capacitor/core';

import type { ZipPluginCapacitor, ZipPluginWrapper, ZipOptions, UnzipOptions, Progress, ProgressCallback } from './definitions';

const Zip = registerPlugin<ZipPluginCapacitor>('Zip', {
    web: () => import('./web').then(m => new m.ZipWeb()),
    electron: () => (window as any).CapacitorCustomPlatform.plugins.Zip,
});

export * from './definitions';
export { Zip };

class ZipWrapper implements ZipPluginWrapper {
    public zip(options: ZipOptions, progress?: ProgressCallback): Promise<Progress> {
        return new Promise((resolve, reject) => {
            Zip.zip(options, (data: any, error: any) => {
                if (!error) {
                    if (!data.completed) {
                        if (progress) {
                            progress(data);
                        }
                    } else {
                        resolve(data);
                    }
                } else {
                    reject(error);
                }
            });
        });
    }

    public unzip(options: UnzipOptions, progress?: ProgressCallback): Promise<Progress> {
        return new Promise((resolve, reject) => {
            // @ts-ignore
            if (window.CapacitorCustomPlatform) {
                Zip.unzip(options).then((callbackId) => {
                    let eventId: any;
                    const callback = (data: any, error: any) => {
                        if (!error) {
                            if (data && !data.complete && progress) {
                                progress(data);
                            } else if (data) {
                                resolve(data);
                                // @ts-ignore
                                window.CapacitorCustomPlatform.plugins.Zip.removeListener(eventId);
                            }
                        } else {
                            reject(error);
                            // @ts-ignore
                            window.CapacitorCustomPlatform.plugins.Zip.removeListener(eventId);
                        }
                    };

                    // @ts-ignore
                    if (window.CapacitorCustomPlatform.plugins.Zip) {
                        // @ts-ignore
                        eventId = window.CapacitorCustomPlatform.plugins.Zip.addListener(callbackId, callback);
                    }
                });
            } else {
                Zip.unzip(options, (data: any, error: any) => {
                    if (!error) {
                        if (!data.complete) {
                            if (progress) {
                                progress(data);
                            }
                        } else {
                            resolve(data);
                        }
                    } else {
                        reject(error);
                    }
                });
            }
        });
    }
}

const ZipPlugin = new ZipWrapper();

export { ZipPlugin };