var capacitorZip = (function (exports, core) {
    'use strict';

    const Zip = core.registerPlugin('Zip', {
        web: () => Promise.resolve().then(function () { return web; }).then(m => new m.ZipWeb()),
        electron: () => window.CapacitorCustomPlatform.plugins.Zip,
    });
    class ZipWrapper {
        zip(options, progress) {
            return new Promise((resolve, reject) => {
                Zip.zip(options, (data, error) => {
                    if (!error) {
                        if (!data.completed) {
                            if (progress) {
                                progress(data);
                            }
                        }
                        else {
                            resolve(data);
                        }
                    }
                    else {
                        reject(error);
                    }
                });
            });
        }
        unzip(options, progress) {
            return new Promise((resolve, reject) => {
                // @ts-ignore
                if (window.CapacitorCustomPlatform) {
                    Zip.unzip(options).then((callbackId) => {
                        let eventId;
                        const callback = (data, error) => {
                            if (!error) {
                                if (data && !data.complete && progress) {
                                    progress(data);
                                }
                                else if (data) {
                                    resolve(data);
                                    // @ts-ignore
                                    window.CapacitorCustomPlatform.plugins.Zip.removeListener(eventId);
                                }
                            }
                            else {
                                reject(error);
                                // @ts-ignore
                                window.CapacitorCustomPlatform.plugins.Zip.removeListener(eventId);
                            }
                        };
                        // @ts-ignore
                        if (window.CapacitorCustomPlatform.plugins.Zip) {
                            // @ts-ignore
                            eventId = window.CapacitorCustomPlatform.plugins.Zip.addListener(callbackId, callback);
                        }
                    });
                }
                else {
                    Zip.unzip(options, (data, error) => {
                        if (!error) {
                            if (!data.complete) {
                                if (progress) {
                                    progress(data);
                                }
                            }
                            else {
                                resolve(data);
                            }
                        }
                        else {
                            reject(error);
                        }
                    });
                }
            });
        }
    }
    const ZipPlugin = new ZipWrapper();

    class ZipWeb extends core.WebPlugin {
        async zip(options, callback) {
            console.error("not implemented in web", options);
            callback(null, "not implemented in web");
            return "";
        }
        async unzip(options, callback) {
            console.error("not implemented in web", options);
            callback(null, "not implemented in web");
            return "";
        }
    }

    var web = /*#__PURE__*/Object.freeze({
        __proto__: null,
        ZipWeb: ZipWeb
    });

    exports.Zip = Zip;
    exports.ZipPlugin = ZipPlugin;

    Object.defineProperty(exports, '__esModule', { value: true });

    return exports;

})({}, capacitorExports);
//# sourceMappingURL=plugin.js.map
