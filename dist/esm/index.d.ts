import type { ZipPluginCapacitor, ZipPluginWrapper, ZipOptions, UnzipOptions, Progress, ProgressCallback } from './definitions';
declare const Zip: ZipPluginCapacitor;
export * from './definitions';
export { Zip };
declare class ZipWrapper implements ZipPluginWrapper {
    zip(options: ZipOptions, progress?: ProgressCallback): Promise<Progress>;
    unzip(options: UnzipOptions, progress?: ProgressCallback): Promise<Progress>;
}
declare const ZipPlugin: ZipWrapper;
export { ZipPlugin };
