export declare type CallbackID = string;
export interface ZipOptions {
    source: string;
    destination: string;
    keepParent?: boolean;
    password?: string;
}
export interface UnzipOptions {
    source: string;
    destination: string;
    overwrite?: boolean;
    password?: string;
}
export interface Progress {
    progress: number;
    complete: boolean;
    path: string;
}
export declare type ProgressCallback = (progress: Progress | null, err?: any) => void;
export interface ZipPluginCapacitor {
    zip(options: ZipOptions, callback?: ProgressCallback): Promise<CallbackID>;
    unzip(options: UnzipOptions, callback?: ProgressCallback): Promise<CallbackID>;
}
export interface ZipPluginWrapper {
    zip(options: ZipOptions, progress?: ProgressCallback): Promise<Progress>;
    unzip(options: UnzipOptions, progress?: ProgressCallback): Promise<Progress>;
}
