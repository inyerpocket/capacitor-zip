import { WebPlugin } from '@capacitor/core';
import type { ZipPluginCapacitor, ZipOptions, UnzipOptions, CallbackID, ProgressCallback } from './definitions';
export declare class ZipWeb extends WebPlugin implements ZipPluginCapacitor {
    zip(options: ZipOptions, callback: ProgressCallback): Promise<CallbackID>;
    unzip(options: UnzipOptions, callback: ProgressCallback): Promise<CallbackID>;
}
