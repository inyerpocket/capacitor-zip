package com.iypsoftware.capacitor.zip;

import com.getcapacitor.JSObject;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import java.util.List;

import android.util.Log;

import com.getcapacitor.annotation.CapacitorPlugin;

@CapacitorPlugin(name = "Zip")
public class ZipPlugin extends Plugin {

    /**
     * @see https://snyk.io/research/zip-slip-vulnerability
     */
    public static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
        File destFile = new File(destinationDir, zipEntry.getName());

        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();

        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
        }

        return destFile;
    }

    @PluginMethod(returnType = PluginMethod.RETURN_CALLBACK)
    public void unzip(PluginCall call) {

      String source = call.getString("source", "");
      String destination = call.getString("destination", "");
      Boolean overwrite = call.getBoolean("overwrite", true);
      String password = call.getString("password");

      if (source.contains("_capacitor_")) {
          source = source.replace("_capacitor_", "");
      }

      if (source.contains("file://")) {
          source = source.replace("file://", "");
      }

      if (destination.contains("_capacitor_")) {
          destination = destination.replace("_capacitor_", "");
      }

      if (destination.contains("file://")) {
          destination = destination.replace("file://", "");
      }

      Log.v("ZipPlugin","begining unzip of source".replace("source", source));

      File archive = new File(source);
      if (!archive.exists()) {
          call.reject("File does not exist, invalid archive path: " + archive.getAbsolutePath());
      }

      try {
          File d = new File(destination);

          if (!d.exists()) {
              d.mkdirs();
          }

          Log.v("ZipPlugin","reading source".replace("source", source));
          final byte[] buffer = new byte[1024];
          final ZipInputStream zis = new ZipInputStream(new FileInputStream(source));
          ZipEntry zipEntry = zis.getNextEntry();
          while (zipEntry != null) {
              final File newFile = newFile(d, zipEntry);
              File destinationParent = newFile.getParentFile();
              //If entry is directory create sub directory on file system
              destinationParent.mkdirs();

              if (!zipEntry.isDirectory())
              {
                  final FileOutputStream fos = new FileOutputStream(newFile);
                  int len;
                  while ((len = zis.read(buffer)) > 0) {
                      fos.write(buffer, 0, len);
                  }
                  fos.close();
              }
              zipEntry = zis.getNextEntry();
          }
          zis.closeEntry();
          zis.close();

          JSObject object = new JSObject();
          object.put("complete", true);
          object.put("progress", 100);
          call.resolve(object);
      } catch (IOException e) {
          call.error(e.getMessage());
      }
    }

    @PluginMethod(returnType = PluginMethod.RETURN_CALLBACK)
    public void zip(PluginCall call) {
      String source = call.getString("source", "");
      String destination = call.getString("destination", "");
      Boolean overwrite = call.getBoolean("overwrite", true);
      String password = call.getString("password");


      if (source.contains("_capacitor_")) {
          source = source.replace("_capacitor_", "");
      }

      if (source.contains("file://")) {
          source = source.replace("file://", "");
      }

      if (destination.contains("_capacitor_")) {
          destination = destination.replace("_capacitor_", "");
      }

      if (destination.contains("file://")) {
          destination = destination.replace("file://", "");
      }

      JSObject object = new JSObject();
      object.put("progress", 100);
      object.put("complete", true);
      object.put("path", destination);
      call.success(object);
    }
}